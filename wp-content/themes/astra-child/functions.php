<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );



add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

add_action( 'wp_enqueue_scripts', function(){

	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
});

function current_year(){
   return date('Y');
}

add_shortcode('fl_year','current_year');

// add_filter( 'facetwp_template_html', function( $output, $class ) {
//     //$GLOBALS['wp_query'] = $class->query;
//     $prod_list = $class->query;  
//     ob_start();       
//    // write_log($class->query_args['check_instock']);
//     if(isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1){

//         $dir = get_stylesheet_directory().'/product-instock-loop-great-floors.php';

//     }else{

//         $dir = get_stylesheet_directory().'/product-loop-great-floors.php';
//     }
   
//     require_once $dir;    
//     return ob_get_clean();
// }, 10, 2 );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

add_filter("wpseo_breadcrumb_links", "override_yoast_breadcrumb_pdp_page");

function override_yoast_breadcrumb_pdp_page($links){
    $post_type = get_post_type();

    if ($post_type === 'luxury_vinyl_tile') {
		$breadcrumb[] = array(
            'url' => get_site_url().'/in-stock-flooring/',
            'text' => 'In Stock Flooring',
        );
        $breadcrumb[] = array(
            'url' => '/in-stock-flooring/luxury-vinyl-tile/',
            'text' => 'Luxury Vinyl Tile'
        );
        array_splice($links, 1, -1, $breadcrumb);
    }
      else if ($post_type === 'laminate_catalog') {
		  $breadcrumb[] = array(
            'url' => get_site_url().'/in-stock-flooring/',
            'text' => 'In Stock Flooring',
        );
        $breadcrumb[] = array(
            'url' => '/in-stock-flooring/laminate/',
            'text' => 'Laminate'
        );
        array_splice($links, 1, -1, $breadcrumb);
    }
    
    else if ($post_type === 'carpeting') {
		 $breadcrumb[] = array(
            'url' => get_site_url().'/in-stock-flooring/',
            'text' => 'In Stock Flooring',
        );
        $breadcrumb[] = array(
            'url' => '/in-stock-flooring/carpet/',
            'text' => 'Carpet'
        );
        array_splice($links, 1, -1, $breadcrumb);
    }
	 //return $links;
    return $links;

    
}

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'facetwp_facet_render_args', function( $args) {
    global $post;
	if ( 'search' == $args['facet']['name'] ) { 
        $current = $post->ID;
        $parent = $post->post_parent;
        if(in_array($current,array('1111121','1111128', '1111130'))){
            $args['facet']['placeholder'] = 'Search In Stock';
        }else{
		    $args['facet']['placeholder'] = 'Search '.get_the_title($parent);
        }
	}
	return $args;
} );

//Custom Banner home page banner hook
function bbtheme_custom_alert_banner_custom() {  

    $website_json =  json_decode(get_option('website_json'));
     
	global $post; 


     foreach($website_json->sites as $site_chat){

        if($site_chat->instance == 'prod'){ 
		
            if ( ($site_chat->banner != null || $site_chat->banner != '') &&  is_front_page() ||  $post->ID == '1589907' ) {

                $content = '<div class="custombanneralert custom" style="background-color:'.$website_json->color1.';">'.$site_chat->banner.'</div>';
                
                return $content;
            }
        }
    }
    
}
add_shortcode('custom_alret_banner', 'bbtheme_custom_alert_banner_custom');